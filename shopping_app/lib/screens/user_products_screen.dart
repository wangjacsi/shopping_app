import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopping_app/providers/products.dart';
import 'package:shopping_app/screens/edit_product_screen.dart';
import 'package:shopping_app/widgets/app_drawer.dart';
import 'package:shopping_app/widgets/user_product_item.dart';

class UserProductsScreen extends StatelessWidget {
  static const routeName = '/user-products';

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<Products>(context, listen: false)
        .fetchAndSetProducts(true);
//        .then((_) {
//      return Future.value();
//    });
//    return Future.value();
  }

  @override
  Widget build(BuildContext context) {
//    final productsData = Provider.of<Products>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(EditProductScreen.routeName);
            },
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: FutureBuilder(
        future: _refreshProducts(context),
        builder: (ctx, snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: () => _refreshProducts(context),
                    child: Consumer<Products>(
                      builder: (ctx, productsData, _) => Padding(
                        padding: EdgeInsets.all(8),
                        child: Consumer<Products>(
                          builder: (context, products, child) => ListView.builder(
                            itemBuilder: (ctx, i) => Column(
                              children: <Widget>[
                                UserProductItem(
                                  products.items[i].id,
                                  products.items[i].title,
                                  products.items[i].imageUrl,
                                ),
                                Divider(),
                              ],
                            ),
                            itemCount: products.items.length,
                          ),
                        ),
                      ),
                    ),
                  ),
      ),
    );
  }
}
