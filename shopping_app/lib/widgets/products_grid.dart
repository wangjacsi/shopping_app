import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopping_app/providers/product.dart';
import 'package:shopping_app/providers/products.dart';
import 'package:shopping_app/widgets/product_item.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFav;

  ProductsGrid(this.showFav);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
//    final productsDataAll = Provider.of<Products>(context).items;
//    final productsDataFavorite = Provider.of<Products>(context).favoriteItems;
    final products = showFav ? productsData.favoriteItems : productsData.items ;// productsDataAll:productsDataFavorite;// productsData.favoriteItems : productsData.items ;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      itemBuilder: (ctx, i) => //ChangeNotifierProvider(
//        builder: (c) => products[i],
          ChangeNotifierProvider.value(
        value: products[i],
        child: ProductItem(
//          id: products[i].id,
//          title: products[i].title,
//          imageUrl: products[i].imageUrl,
            ),
      ),
      padding: const EdgeInsets.all(10),
      itemCount: products.length,
    );
  }
}
