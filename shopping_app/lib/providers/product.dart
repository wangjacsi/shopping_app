import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier{
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    this.isFavorite = false,
    @required this.imageUrl,
    @required this.title,
    @required this.description,
    @required this.id,
    @required this.price,
  });

  void _setFavoriteValue(bool value) {
    isFavorite = value;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(String token, String userId) async {
    final oldStatus = isFavorite;
    _setFavoriteValue(!isFavorite);

    final url = 'https://shopping-app-42c82.firebaseio.com/userFavorites/$userId/$id.json?auth=$token';

    try {
      final response = await http.put(url, body: json.encode(
        isFavorite,
      ));
      if(response.statusCode >= 400) {
        _setFavoriteValue(oldStatus);
      }
    } catch(error) {
      _setFavoriteValue(oldStatus);
    }

  }
}
